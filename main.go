// STUDENT - An Early AI Program that solves simple arithmetic problems
// The following is the implementation of the STUDENT program in Go
package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"student/solver"
	"github.com/gin-gonic/gin"
	"github.com/gin-contrib/cors"
)

// ConfigRuntime sets the number of operating system threads.
func ConfigRuntime() {
	fmt.Println("Configuring runtime...")
	nuCPU := runtime.NumCPU()
	runtime.GOMAXPROCS(nuCPU)
	fmt.Printf("Running with %d CPUs\n", nuCPU)
}

// StartGin starts gin web server with setting router.
func StartGin() {
	gin.SetMode(gin.ReleaseMode)

	fmt.Println("Starting gin web server...")
	router := gin.New()
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	router.Use(cors.New(config))

	// GET request to /paip/student/:encodedInput
	// encodedInput is a base64 encoded string of the problem
	// to be solved by the STUDENT program
	//
	// Returns a JSON object with the following fields:
	// 		- problem:     the original problem
	// 		- infixEqns:   parsed problem in equations 
	// 					   in infix notation
	// 		- substituted: the equations with substituted
	// 					   variables and constants
	// 		- solved:      the solved equations (or) results
	router.GET("/paip/student/:encodedInput", func(c *gin.Context) {
		encodedInput := c.Param("encodedInput")
		decodedInput, err := url.PathUnescape(encodedInput)
		if err != nil {
			log.Panicf("error: %s", err)
		}

		problem := string(decodedInput)
		infixEqns, substituted, solved := solver.Student(problem)
		c.IndentedJSON(http.StatusOK, gin.H{
			"problem": problem,
			"infixEqns": infixEqns,
			"substituted": substituted,
			"solved": solved,
		})
	})

	// Listen on port 8080
	fmt.Println("Listening on port 8080...")
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	if err := router.Run(":" + port); err != nil {
        log.Panicf("error: %s", err)
	}
}

func main() {
	ConfigRuntime()
	StartGin()
}
